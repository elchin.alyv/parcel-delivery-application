package com.elchin.apigateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RequestLogger extends ZuulFilter {
    Logger log = LoggerFactory.getLogger(RequestLogger.class);

    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        log.debug("Request method: {}, Request Uri: {} ", ctx.getRequest().getMethod(), ctx.getRequest().getRequestURI());
        return null;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }
}
