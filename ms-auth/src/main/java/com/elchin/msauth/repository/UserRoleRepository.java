package com.elchin.msauth.repository;

import com.elchin.msauth.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

    @Query("select ur from UserRole ur inner join ur.user u " +
            "inner join ur.role r where upper(u.username) = upper(:username) and ur.active = true and u.active = true and  r.active = true")
    List<UserRole> getActiveUserRolesByUsername(String username);
}
