package com.elchin.msauth.repository;

import com.elchin.msauth.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    boolean existsByUsername(String username);

    Optional<User> findByUsernameIgnoreCaseAndActive(String username, Boolean active);

    @Query("select u from User u inner join u.userRoles ur " +
            "inner join ur.role r where r.roleName = :role and u.active = true and ur.active = true and r.active = true")
    List<User> getAllActiveUsersByTypeAndActive(String role);
}
