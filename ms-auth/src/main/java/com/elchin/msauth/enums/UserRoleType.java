package com.elchin.msauth.enums;

public enum UserRoleType {
    USER,
    COURIER,
    ADMIN;
}
