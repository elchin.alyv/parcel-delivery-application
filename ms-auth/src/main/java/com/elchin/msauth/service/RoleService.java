package com.elchin.msauth.service;

import com.elchin.msauth.entity.Role;

public interface RoleService {

    Role getRoleByRoleName(String roleName);
}
