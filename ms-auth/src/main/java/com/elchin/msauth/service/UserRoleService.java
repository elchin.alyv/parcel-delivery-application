package com.elchin.msauth.service;

import com.elchin.msauth.entity.UserRole;

import java.util.List;

public interface UserRoleService {

    UserRole saveUserRole(UserRole userRole);

    List<UserRole> getUserRoleByUsername(String username);
}
