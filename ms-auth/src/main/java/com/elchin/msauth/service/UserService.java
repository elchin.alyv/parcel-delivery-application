package com.elchin.msauth.service;

import com.elchin.msauth.dto.response.UserResponseDto;
import com.elchin.msauth.entity.User;
import com.elchin.msauth.enums.UserRoleType;

import java.util.List;
import java.util.Optional;

public interface UserService {

    boolean checkUsernameExist(String username);

    User saveUser(User user);

    Optional<User> getUserByUsernameAndActive(String username, boolean active);

    List<UserResponseDto> getAllActiveUsersByType(UserRoleType userType);
}
