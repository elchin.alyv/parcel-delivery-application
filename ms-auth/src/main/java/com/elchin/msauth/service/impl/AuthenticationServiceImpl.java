package com.elchin.msauth.service.impl;

import com.elchin.msauth.dto.request.CreateUserRequestDto;
import com.elchin.msauth.dto.request.JwtRequestDto;
import com.elchin.msauth.dto.request.ValidateJwtRequestDto;
import com.elchin.msauth.dto.response.JwtResponseDto;
import com.elchin.msauth.dto.response.ValidateJwtResponseDto;
import com.elchin.msauth.entity.Role;
import com.elchin.msauth.entity.User;
import com.elchin.msauth.entity.UserRole;
import com.elchin.msauth.enums.UserRoleType;
import com.elchin.msauth.error.exception.CommonInternalException;
import com.elchin.msauth.error.exception.CommonUserException;
import com.elchin.msauth.security.JwtTokenUtil;
import com.elchin.msauth.service.AuthenticationService;
import com.elchin.msauth.service.RoleService;
import com.elchin.msauth.service.UserRoleService;
import com.elchin.msauth.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final UserService userService;
    private final RoleService roleService;
    private final UserRoleService userRoleService;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenUtil jwtTokenUtil;
    private final AuthenticationManager authenticationManager;

    @Override
    public void signUpUser(CreateUserRequestDto requestDto, UserRoleType userType) {
        if (userService.checkUsernameExist(requestDto.getUsername())) {
            throw new CommonUserException(400, "Username already used: " + requestDto.getUsername());
        }
        Role role = roleService.getRoleByRoleName(userType.name());
        if (role == null) {
            throw new CommonInternalException(500, "Role can not find: " + UserRoleType.USER.name());
        }

        User user = User.builder().username(requestDto.getUsername())
                .firstName(requestDto.getFirstName())
                .lastName(requestDto.getLastName())
                .password(passwordEncoder.encode(requestDto.getPassword()))
                .active(true).build();
        user = userService.saveUser(user);

        UserRole userRole = UserRole.builder().user(user)
                .role(role)
                .active(true).build();
        userRoleService.saveUserRole(userRole);

    }

    @Override
    public JwtResponseDto signInUser(JwtRequestDto requestDto) {
        String username = requestDto.getUsername();
        String password = requestDto.getPassword();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        final String jwtToken = jwtTokenUtil.generateToken(username);
        String refreshToken = UUID.randomUUID().toString(); //todo can save db, cache or else to generate new jwt token
        return new JwtResponseDto(jwtToken, refreshToken);
    }

    @Override
    public ValidateJwtResponseDto validateJwt(ValidateJwtRequestDto requestDto) {
        String jwt = requestDto.getJwtToken();
        Set<String> roles = requestDto.getRoles();
        if (jwt.startsWith("Bearer ") && jwtTokenUtil.validateToken(jwt.substring(7))) {
            String username = jwtTokenUtil.getUsernameFromToken(jwt.substring(7));
            List<UserRole> userRoleList = userRoleService.getUserRoleByUsername(username);
            Set<String> userRoles = new HashSet<>();
            userRoleList.forEach(userRole -> {
                if (roles.contains(userRole.getRole().getRoleName())) {
                    userRoles.add(userRole.getRole().getRoleName());
                }
            });
            if (!userRoles.isEmpty()) {
                return new ValidateJwtResponseDto(true, username, userRoles);
            }
        }
        return ValidateJwtResponseDto.builder().success(false).build();
    }


}
