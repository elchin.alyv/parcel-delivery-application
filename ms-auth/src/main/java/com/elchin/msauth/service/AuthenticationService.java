package com.elchin.msauth.service;

import com.elchin.msauth.dto.request.CreateUserRequestDto;
import com.elchin.msauth.dto.request.JwtRequestDto;
import com.elchin.msauth.dto.request.ValidateJwtRequestDto;
import com.elchin.msauth.dto.response.JwtResponseDto;
import com.elchin.msauth.dto.response.ValidateJwtResponseDto;
import com.elchin.msauth.enums.UserRoleType;

public interface AuthenticationService {

    void signUpUser(CreateUserRequestDto createUserRequestDto, UserRoleType userType);

    JwtResponseDto signInUser(JwtRequestDto requestDto);

    ValidateJwtResponseDto validateJwt(ValidateJwtRequestDto requestDto);
}
