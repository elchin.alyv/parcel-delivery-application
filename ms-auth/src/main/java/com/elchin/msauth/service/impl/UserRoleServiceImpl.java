package com.elchin.msauth.service.impl;

import com.elchin.msauth.entity.UserRole;
import com.elchin.msauth.repository.UserRoleRepository;
import com.elchin.msauth.service.UserRoleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class UserRoleServiceImpl implements UserRoleService {

    private final UserRoleRepository userRoleRepository;

    @Override
    public UserRole saveUserRole(UserRole userRole) {
        return userRoleRepository.save(userRole);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserRole> getUserRoleByUsername(String username) {
        return userRoleRepository.getActiveUserRolesByUsername(username);
    }
}
