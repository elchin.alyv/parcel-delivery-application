package com.elchin.msauth.service.impl;

import com.elchin.msauth.dto.response.UserResponseDto;
import com.elchin.msauth.entity.User;
import com.elchin.msauth.enums.UserRoleType;
import com.elchin.msauth.mapper.UserMapper;
import com.elchin.msauth.repository.UserRepository;
import com.elchin.msauth.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Transactional(readOnly = true)
    public boolean checkUsernameExist(String username) {
        return userRepository.existsByUsername(username);
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<User> getUserByUsernameAndActive(String username, boolean active) {
        return userRepository.findByUsernameIgnoreCaseAndActive(username, active);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserResponseDto> getAllActiveUsersByType(UserRoleType userType) {
        List<User> users = userRepository.getAllActiveUsersByTypeAndActive(userType.name());
        return users.stream().map(UserMapper.INSTANCE::toDto).collect(Collectors.toList());
    }
}
