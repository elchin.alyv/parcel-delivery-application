package com.elchin.msauth.dto.request;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidateJwtRequestDto implements Serializable {

    @NotEmpty(message = "Jwt must not be empty")
    private String jwtToken;
    @NotNull(message = "Roles must not be null")
    private Set<String> roles;

}
