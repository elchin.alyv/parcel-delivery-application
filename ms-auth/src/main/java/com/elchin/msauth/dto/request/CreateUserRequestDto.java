package com.elchin.msauth.dto.request;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserRequestDto implements Serializable {

    @Email(message = "Username should be email")
    private String username;

    @NotEmpty(message = "Password must not be empty")
    @Size(min = 8, max = 30)
    private String password;

    @NotEmpty(message = "First name must not be empty")
    private String firstName;

    @NotEmpty(message = "Last name must not be empty")
    private String lastName;

}
