package com.elchin.msauth.dto.request;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JwtRequestDto implements Serializable {

	@NotEmpty(message = "Username must not be empty")
	private String username;
	@NotEmpty(message = "Password must not be empty")
	private String password;


}