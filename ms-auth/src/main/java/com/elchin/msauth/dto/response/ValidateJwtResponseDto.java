package com.elchin.msauth.dto.response;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidateJwtResponseDto implements Serializable {

    private boolean success;
    private String username;
    private Set<String> roles;

}
