package com.elchin.msauth.dto.response;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JwtResponseDto implements Serializable {

    private String jwtToken;
    private String refreshToken;

}