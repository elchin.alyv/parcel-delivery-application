package com.elchin.msauth.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
@RequiredArgsConstructor
public class JwtRequestFilter extends OncePerRequestFilter {

    private final JwtTokenUtil jwtTokenUtil;
    private final JwtUserDetailsService jwtUserDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        log.debug("JwtTokenFilter started, {} , {}", request.getMethod(), request.getRequestURI());
        final String requestJwt = request.getHeader("Authorization");

        String username = null;
        String jwtToken = null;

        if (requestJwt != null && requestJwt.startsWith("Bearer ")) {
            jwtToken = requestJwt.substring(7);

            if (jwtTokenUtil.validateToken(jwtToken)) {
                username = jwtTokenUtil.getUsernameFromToken(jwtToken);
                UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(username);
                Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);
                log.debug("success login for: {} ", username);
            }

        }

        chain.doFilter(request, response);

    }
}
