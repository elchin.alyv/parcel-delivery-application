package com.elchin.msauth.controller;

import com.elchin.msauth.dto.request.CreateUserRequestDto;
import com.elchin.msauth.dto.request.JwtRequestDto;
import com.elchin.msauth.dto.request.ValidateJwtRequestDto;
import com.elchin.msauth.enums.UserRoleType;
import com.elchin.msauth.service.AuthenticationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
@Api(value = "Authentication and Sign up services")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @ApiOperation(value = "Service for user sign up")
    @PostMapping("/signup")
    public ResponseEntity<?> signup(@RequestBody @Valid CreateUserRequestDto requestDto) {
        authenticationService.signUpUser(requestDto, UserRoleType.USER);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @ApiOperation(value = "Service for courier sign up")
    @PostMapping("/signup/courier")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> createCourier(@RequestBody @Valid CreateUserRequestDto requestDto) {
        authenticationService.signUpUser(requestDto, UserRoleType.COURIER);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @ApiOperation(value = "Service for user sign in")
    @PostMapping("/signin")
    public ResponseEntity<?> signin(@RequestBody @Valid JwtRequestDto requestDto) {
        return ResponseEntity.ok(authenticationService.signInUser(requestDto));
    }

    @ApiOperation(value = "Service for validate token among microservices")
    @PostMapping("/validate")
    public ResponseEntity<?> validateToken(@RequestBody @Valid ValidateJwtRequestDto requestDto) {
        return ResponseEntity.ok(authenticationService.validateJwt(requestDto));
    }
}
