package com.elchin.msauth.controller;

import com.elchin.msauth.enums.UserRoleType;
import com.elchin.msauth.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
@Api(value = "User related services")
public class UserController {

    private final UserService userService;

    @ApiOperation(value = "Get all couriers")
    @GetMapping("/couriers")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getAllCouriers() {
        return ResponseEntity.ok(userService.getAllActiveUsersByType(UserRoleType.COURIER));
    }

}
