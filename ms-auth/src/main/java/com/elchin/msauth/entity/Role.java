package com.elchin.msauth.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "roles")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Role extends AbstractEntity implements Serializable {

    @Column(name = "role_name", nullable = false)
    private String roleName;

    @Column(name = "active", nullable = false)
    private Boolean active;

    @Builder.Default
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "role")
    private Set<UserRole> userRoles = new HashSet<>();


}
