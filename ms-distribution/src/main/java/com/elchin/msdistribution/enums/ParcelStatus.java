package com.elchin.msdistribution.enums;

public enum ParcelStatus {
    CREATED,
    ACCEPTED,
    PENDING,
    IN_PROGRESS,
    DELIVERED,
    CANCELED;
}
