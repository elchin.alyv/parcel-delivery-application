package com.elchin.msdistribution.messaging.event;

import com.elchin.msdistribution.enums.ParcelStatus;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderStatusEvent implements Serializable {

    private Long orderId;
    private ParcelStatus status;

}
