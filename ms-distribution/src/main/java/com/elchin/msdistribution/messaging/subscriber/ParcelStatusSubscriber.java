package com.elchin.msdistribution.messaging.subscriber;

import com.elchin.msdistribution.config.RabbitMQConfig;
import com.elchin.msdistribution.messaging.event.ParcelStatusEvent;
import com.elchin.msdistribution.service.ParcelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class ParcelStatusSubscriber {

    private final ParcelService parcelService;

    @RabbitListener(queues = RabbitMQConfig.PARCEL_STATUS_QUEUE)
    public void consumeMessageFromQueue(ParcelStatusEvent parcelStatusEvent) {
        log.debug("Parcel status event consumed, {} ", parcelStatusEvent);
        parcelService.changeParcelStatus(parcelStatusEvent);
    }
}
