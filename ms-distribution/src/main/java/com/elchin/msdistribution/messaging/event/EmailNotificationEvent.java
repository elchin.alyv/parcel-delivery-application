package com.elchin.msdistribution.messaging.event;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EmailNotificationEvent implements Serializable {
    private String to;
    private String subject;
    private String body;
}
