package com.elchin.msdistribution.messaging.publisher;

import com.elchin.msdistribution.config.RabbitMQConfig;
import com.elchin.msdistribution.messaging.event.OrderStatusEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class OrderStatusPublisher {

    private final RabbitTemplate rabbitTemplate;

    public void publishOrderStatus(OrderStatusEvent orderStatusEvent) {
        log.debug("Order status publish started, {}",orderStatusEvent);
        rabbitTemplate.convertAndSend(RabbitMQConfig.ORDER_STATUS_QUEUE, orderStatusEvent);
        log.debug("Order status published, {}", orderStatusEvent);
    }
}
