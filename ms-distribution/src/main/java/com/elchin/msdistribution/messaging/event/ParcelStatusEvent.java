package com.elchin.msdistribution.messaging.event;

import com.elchin.msdistribution.enums.ParcelStatus;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ParcelStatusEvent {

    private Long orderId;
    private String orderName;
    private String destination;
    private BigDecimal amount;
    private ParcelStatus status;
    private String customerUsername;
    private String courierUsername;

}
