package com.elchin.msdistribution.client.dto.request;

import lombok.*;

import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidateJwtRequestDto implements Serializable {

    private String jwtToken;
    private Set<String> roles;

}
