package com.elchin.msdistribution.client.dto.response;

import lombok.*;

import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidateJwtResponseDto implements Serializable {

    private boolean success;
    private String username;
    private Set<String> roles;

}
