package com.elchin.msdistribution.error.exception;

import lombok.Getter;

@Getter
public class CommonUserException extends RuntimeException{

    private final Integer code;
    private final String message;

    public CommonUserException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
