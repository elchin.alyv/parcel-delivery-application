package com.elchin.msdistribution.error.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class ErrorResponse implements Serializable {

    private final Integer code;
    private final List<String> messageList;

    public ErrorResponse(Integer code, List<String> messageList) {
        this.code = code;
        this.messageList = messageList;
    }
}
