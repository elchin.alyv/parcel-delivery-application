package com.elchin.msdistribution.error;


import com.elchin.msdistribution.error.exception.CommonInternalException;
import com.elchin.msdistribution.error.exception.CommonUserException;
import com.elchin.msdistribution.error.response.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(CommonUserException.class)
    public ResponseEntity<?> handleCommonUserException(CommonUserException ex) {
        ErrorResponse errorResponse = new ErrorResponse(ex.getCode(), List.of(ex.getMessage()));
        return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(ex.getCode()));
    }

    @ExceptionHandler(CommonInternalException.class)
    public ResponseEntity<?> handleCommonInternalException(CommonInternalException ex) {
        ex.printStackTrace();
        log.error(ex.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(500, List.of("Internal exception occurred"));
        return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(500));
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<?> handleBadCredentialsException(BadCredentialsException ex) {
        ErrorResponse errorResponse = new ErrorResponse(400, List.of(ex.getMessage()));
        return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(400));
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> handleAccessDeniedException(AccessDeniedException ex) {
        ErrorResponse errorResponse = new ErrorResponse(401, List.of("UNAUTHORIZED"));
        return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(401));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> messageList = ex.getBindingResult().getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList());
        ErrorResponse errorResponse = new ErrorResponse(400, messageList);
        return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(400));
    }

    @Override
    protected ResponseEntity<Object> handleMissingPathVariable(MissingPathVariableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(400, List.of(ex.getMessage()));
        return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(400));
    }

    @Override
    protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(400, List.of(ex.getMessage()));
        return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(400));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception ex) {
        ex.printStackTrace();
        log.error(ex.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(500, List.of("Internal exception occurred"));
        return new ResponseEntity<>(errorResponse, HttpStatus.valueOf(500));
    }

}
