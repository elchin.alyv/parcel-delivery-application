package com.elchin.msdistribution.service;

import com.elchin.msdistribution.dto.response.ParcelResponseDto;
import com.elchin.msdistribution.entity.Parcel;
import com.elchin.msdistribution.messaging.event.ParcelStatusEvent;

import java.util.List;
import java.util.Optional;

public interface ParcelService {

    void changeParcelStatus(ParcelStatusEvent parcelStatusEvent);

    Parcel saveParcel(Parcel parcel);

    Optional<Parcel> getParcelByOrderId(Long id);

    ParcelResponseDto getParcelById(Long id);

    Optional<Parcel> getParcelByIdAndActive(Long id, boolean active);

    Optional<Parcel> getParcelByIdAndCourierUsernameAndActive(Long id, String username, boolean active);

    List<ParcelResponseDto> getAllParcels();

    List<Parcel> getAllParcelsByActive(boolean active);

    List<Parcel> getAllParcelsByCourierUsernameAndActive(String username, boolean active);

    void changeStatusToInProgress(Long id);

    void changeStatusToDelivered(Long id);
}
