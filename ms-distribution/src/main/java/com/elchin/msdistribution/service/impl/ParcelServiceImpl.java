package com.elchin.msdistribution.service.impl;

import com.elchin.msdistribution.dto.response.ParcelResponseDto;
import com.elchin.msdistribution.entity.Parcel;
import com.elchin.msdistribution.enums.ParcelStatus;
import com.elchin.msdistribution.error.exception.CommonInternalException;
import com.elchin.msdistribution.error.exception.CommonUserException;
import com.elchin.msdistribution.mapper.ParcelMapper;
import com.elchin.msdistribution.messaging.event.OrderStatusEvent;
import com.elchin.msdistribution.messaging.event.ParcelStatusEvent;
import com.elchin.msdistribution.messaging.publisher.OrderStatusPublisher;
import com.elchin.msdistribution.repository.ParcelRepository;
import com.elchin.msdistribution.security.model.AuthUser;
import com.elchin.msdistribution.service.EmailService;
import com.elchin.msdistribution.service.ParcelService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class ParcelServiceImpl implements ParcelService {

    private final ParcelRepository parcelRepository;
    private final OrderStatusPublisher orderStatusPublisher;
    private final EmailService emailService;
    private final AuthUser authUser;

    @Override
    public void changeParcelStatus(ParcelStatusEvent parcelStatusEvent) {
        Long orderId = parcelStatusEvent.getOrderId();
        if (ParcelStatus.PENDING.equals(parcelStatusEvent.getStatus())) {
            Parcel parcel = Parcel.builder().orderId(orderId)
                    .orderName(parcelStatusEvent.getOrderName())
                    .amount(parcelStatusEvent.getAmount())
                    .destination(parcelStatusEvent.getDestination())
                    .courierUsername(parcelStatusEvent.getCourierUsername())
                    .customerUsername(parcelStatusEvent.getCustomerUsername())
                    .status(parcelStatusEvent.getStatus())
                    .active(true).build();

            saveParcel(parcel);
        } else if (ParcelStatus.CANCELED.equals(parcelStatusEvent.getStatus())) {
            Parcel parcel = getParcelByOrderId(orderId)
                    .orElseThrow(() -> new CommonInternalException(500, "Order id can not found: Order id: " + orderId));
            parcel.setStatus(ParcelStatus.CANCELED);
            saveParcel(parcel);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Parcel saveParcel(Parcel parcel) {
        return parcelRepository.save(parcel);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Parcel> getParcelByOrderId(Long id) {
        return parcelRepository.getParcelByOrderId(id);
    }

    @Override
    public ParcelResponseDto getParcelById(Long id) {
        Optional<Parcel> parcelOptional = authUser.getRoles().contains("ADMIN") ?
                getParcelByIdAndActive(id, true)
                :
                getParcelByIdAndCourierUsernameAndActive(id, authUser.getUsername(), true);
        Parcel parcel = parcelOptional.orElseThrow(() -> new CommonUserException(400, "Parcel not found, Parcel id: " + id));
        return ParcelMapper.INSTANCE.toDto(parcel);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Parcel> getParcelByIdAndActive(Long id, boolean active) {
        return parcelRepository.getParcelByIdAndActive(id, active);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Parcel> getParcelByIdAndCourierUsernameAndActive(Long id, String username, boolean active) {
        return parcelRepository.getParcelByIdAndCourierUsernameAndActive(id, username, active);
    }

    @Override
    public List<ParcelResponseDto> getAllParcels() {
        List<Parcel> parcels = authUser.getRoles().contains("ADMIN") ?
                getAllParcelsByActive(true)
                :
                getAllParcelsByCourierUsernameAndActive(authUser.getUsername(), true);

        return parcels.stream().map(ParcelMapper.INSTANCE::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Parcel> getAllParcelsByActive(boolean active) {
        return parcelRepository.getAllByActive(active);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Parcel> getAllParcelsByCourierUsernameAndActive(String username, boolean active) {
        return parcelRepository.getAllByCourierUsernameAndActive(username, active);
    }

    @Override
    public void changeStatusToInProgress(Long id) {
        Parcel parcel = getParcelByIdAndCourierUsernameAndActive(id, authUser.getUsername(), true)
                .orElseThrow(() -> new CommonUserException(400, "Parcel not found, Parcel id: " + id));

        if (!ParcelStatus.PENDING.equals(parcel.getStatus())) {
            throw new CommonUserException(400, "You can't change to in-progress, Parcel Status: " + parcel.getStatus());
        }

        parcel.setStatus(ParcelStatus.IN_PROGRESS);
        parcel = saveParcel(parcel);
        publishOrderStatus(parcel.getOrderId(), parcel.getStatus());

        String parcelName = parcel.getOrderName() + " parcel";
        emailService.sendEmail(parcel.getCustomerUsername(), parcelName, "Your " + parcelName + " on the way");
    }

    @Override
    public void changeStatusToDelivered(Long id) {
        Parcel parcel = getParcelByIdAndCourierUsernameAndActive(id, authUser.getUsername(), true)
                .orElseThrow(() -> new CommonUserException(400, "Parcel not found, Parcel id: " + id));

        if (!ParcelStatus.IN_PROGRESS.equals(parcel.getStatus())) {
            throw new CommonUserException(400, "You can't change to delivered, Parcel Status: " + parcel.getStatus());
        }

        parcel.setStatus(ParcelStatus.DELIVERED);
        parcel = saveParcel(parcel);
        publishOrderStatus(parcel.getOrderId(), parcel.getStatus());

        String parcelName = parcel.getOrderName() + " parcel";
        emailService.sendEmail(parcel.getCustomerUsername(), parcelName, "Your " + parcelName + " is delivered");
    }

    private void publishOrderStatus(Long orderId, ParcelStatus parcelStatus) {
        OrderStatusEvent orderStatusEvent = OrderStatusEvent.builder().orderId(orderId)
                .status(parcelStatus).build();
        orderStatusPublisher.publishOrderStatus(orderStatusEvent);
    }
}
