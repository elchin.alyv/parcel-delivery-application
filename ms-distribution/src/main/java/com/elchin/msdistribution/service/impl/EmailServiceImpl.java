package com.elchin.msdistribution.service.impl;

import com.elchin.msdistribution.messaging.event.EmailNotificationEvent;
import com.elchin.msdistribution.messaging.publisher.NotificationPublisher;
import com.elchin.msdistribution.service.EmailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final NotificationPublisher notificationPublisher;

    @Override
    public void sendEmail(String to, String subject, String body) {
        EmailNotificationEvent emailNotificationEvent = EmailNotificationEvent.builder().to(to)
                .subject(subject)
                .body(body).build();
        notificationPublisher.publishNotification(emailNotificationEvent);
    }
}
