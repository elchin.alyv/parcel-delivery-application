package com.elchin.msdistribution.dto.response;

import com.elchin.msdistribution.enums.ParcelStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParcelResponseDto implements Serializable {

    private Long id;
    private Long orderId;
    private String orderName;
    private String destination;
    private ParcelStatus status;
    private BigDecimal amount;
    private String customerUsername;
    private String courierUsername;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    private Boolean active;
}
