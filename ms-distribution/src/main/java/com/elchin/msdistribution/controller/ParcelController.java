package com.elchin.msdistribution.controller;

import com.elchin.msdistribution.service.ParcelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/parcels")
@RequiredArgsConstructor
@Validated
@Api(value = "Parcel related services")
public class ParcelController {

    private final ParcelService parcelService;

    @ApiOperation(value = "Service for get parcel detail")
    @GetMapping("/{id}")
    @PreAuthorize("@authService.checkUserAccessToResource(#jwt,'COURIER','ADMIN')")
    public ResponseEntity<?> getParcelDetailById(@PathVariable("id") @NotNull(message = "Id can not null") Long parcelId,
                                                 @RequestHeader("Authorization") @NotBlank(message = "Token expected") String jwt) {
        return ResponseEntity.ok(parcelService.getParcelById(parcelId));
    }

    @ApiOperation(value = "Service for get all parcels")
    @GetMapping
    @PreAuthorize("@authService.checkUserAccessToResource(#jwt,'COURIER','ADMIN')")
    public ResponseEntity<?> getAllParcels(@RequestHeader("Authorization") @NotBlank(message = "Token expected") String jwt) {
        return ResponseEntity.ok(parcelService.getAllParcels());
    }

    @ApiOperation(value = "Service for change parcel status to in-progress ")
    @PutMapping("/{id}/in-progress")
    @PreAuthorize("@authService.checkUserAccessToResource(#jwt,'COURIER')")
    public ResponseEntity<?> changeToParcelToInProgress(@PathVariable("id") @NotNull(message = "Id can not null") Long parcelId,
                                                        @RequestHeader("Authorization") @NotBlank(message = "Token expected") String jwt) {
        parcelService.changeStatusToInProgress(parcelId);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "Service for change parcel status to delivered ")
    @PutMapping("/{id}/delivered")
    @PreAuthorize("@authService.checkUserAccessToResource(#jwt,'COURIER')")
    public ResponseEntity<?> changeToParcelToDelivered(@PathVariable("id") @NotNull(message = "Id can not null") Long parcelId,
                                                        @RequestHeader("Authorization") @NotBlank(message = "Token expected") String jwt) {
        parcelService.changeStatusToDelivered(parcelId);
        return ResponseEntity.ok().build();
    }

}
