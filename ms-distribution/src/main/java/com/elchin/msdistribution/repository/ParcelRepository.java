package com.elchin.msdistribution.repository;

import com.elchin.msdistribution.entity.Parcel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ParcelRepository extends JpaRepository<Parcel, Long> {

    Optional<Parcel> getParcelByOrderId(Long id);

    Optional<Parcel> getParcelByIdAndActive(Long id, boolean active);

    Optional<Parcel> getParcelByIdAndCourierUsernameAndActive(Long id, String username, boolean active);

    List<Parcel> getAllByActive(boolean active);

    List<Parcel> getAllByCourierUsernameAndActive(String username, boolean active);
}
