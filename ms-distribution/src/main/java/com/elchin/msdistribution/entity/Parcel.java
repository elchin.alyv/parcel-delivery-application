package com.elchin.msdistribution.entity;

import com.elchin.msdistribution.enums.ParcelStatus;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "parcels")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Parcel extends AbstractEntity implements Serializable {

    @Column(name = "order_id", nullable = false)
    private Long orderId;

    @Column(name = "order_name", nullable = false)
    private String orderName;

    @Column(name = "destination", nullable = false)
    private String destination;

    @Column(name = "order_amount", nullable = false)
    private BigDecimal amount;

    @Column(name = "coordinate")
    private String coordinate;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private ParcelStatus status;

    @Column(name = "customer_username", nullable = false)
    private String customerUsername;

    @Column(name = "courier_username", nullable = false)
    private String courierUsername;

    @Column(name = "active", nullable = false)
    private Boolean active;

}
