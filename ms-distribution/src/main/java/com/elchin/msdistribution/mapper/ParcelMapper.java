package com.elchin.msdistribution.mapper;

import com.elchin.msdistribution.dto.response.ParcelResponseDto;
import com.elchin.msdistribution.entity.Parcel;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ParcelMapper {
    ParcelMapper INSTANCE = Mappers.getMapper(ParcelMapper.class);

    ParcelResponseDto toDto(Parcel parcel);
}
