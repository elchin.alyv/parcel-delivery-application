package com.elchin.msdistribution;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MsDistributionApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsDistributionApplication.class, args);
	}

}
