# Parcel Delivery Application

It contains 5 microservices:

1) api-gateway (Api gateway for microservices)

2) ms-auth (For authentication and sign up)

3) ms-order (Dealing with orders)

4) ms-distribition (Dealing with parcel distribitions)

5) ms-notification (Dealing with user notifications)

## How to run

Just run start.sh file, and it will build all required files and will start the applications. For testing postman collection added.  

## Application architecture


![application-architecture](./application-architecture.png)