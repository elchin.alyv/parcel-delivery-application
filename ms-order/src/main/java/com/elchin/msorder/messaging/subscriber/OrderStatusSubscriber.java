package com.elchin.msorder.messaging.subscriber;

import com.elchin.msorder.config.RabbitMQConfig;
import com.elchin.msorder.messaging.event.OrderStatusEvent;
import com.elchin.msorder.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class OrderStatusSubscriber {

    private final OrderService orderService;

    @RabbitListener(queues = RabbitMQConfig.ORDER_STATUS_QUEUE)
    public void consumeMessageFromQueue(OrderStatusEvent orderStatusEvent) {
        log.debug("Order status event consumed, {} ", orderStatusEvent);
        orderService.changeOrderStatus(orderStatusEvent);
    }
}
