package com.elchin.msorder.messaging.event;

import com.elchin.msorder.enums.OrderStatus;
import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ParcelStatusEvent {

    private Long orderId;
    private String orderName;
    private String destination;
    private BigDecimal amount;
    private OrderStatus status;
    private String customerUsername;
    private String courierUsername;

}
