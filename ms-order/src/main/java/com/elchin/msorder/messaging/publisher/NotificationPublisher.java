package com.elchin.msorder.messaging.publisher;

import com.elchin.msorder.config.RabbitMQConfig;
import com.elchin.msorder.messaging.event.EmailNotificationEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class NotificationPublisher {

    private final RabbitTemplate rabbitTemplate;

    public void publishNotification(EmailNotificationEvent emailNotificationEvent) {
        log.debug("Email notification publish started");
        rabbitTemplate.convertAndSend(RabbitMQConfig.NOTIFICATION_QUEUE, emailNotificationEvent);
        log.debug("Email notification published, {}", emailNotificationEvent);
    }
}
