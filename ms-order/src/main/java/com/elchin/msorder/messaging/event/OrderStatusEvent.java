package com.elchin.msorder.messaging.event;

import com.elchin.msorder.enums.OrderStatus;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OrderStatusEvent implements Serializable {

    private Long orderId;
    private OrderStatus status;

}
