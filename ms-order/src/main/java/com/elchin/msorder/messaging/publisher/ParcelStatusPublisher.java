package com.elchin.msorder.messaging.publisher;

import com.elchin.msorder.config.RabbitMQConfig;
import com.elchin.msorder.messaging.event.EmailNotificationEvent;
import com.elchin.msorder.messaging.event.ParcelStatusEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class ParcelStatusPublisher {

    private final RabbitTemplate rabbitTemplate;

    public void publishParcelStatus(ParcelStatusEvent parcelStatusEvent) {
        log.debug("Parcel status publish started, {}", parcelStatusEvent);
        rabbitTemplate.convertAndSend(RabbitMQConfig.PARCEL_STATUS_QUEUE, parcelStatusEvent);
        log.debug("Parcel status published, {}", parcelStatusEvent);
    }
}
