package com.elchin.msorder.util;

import java.math.BigDecimal;

public class OrderUtil {

    private static final BigDecimal FEE_PER_WEIGHT = BigDecimal.valueOf(2.5); // 1 kg = 2.5 azn

    public static BigDecimal calculateOrderAmount(double weight) {
        return FEE_PER_WEIGHT.multiply(BigDecimal.valueOf(weight));
    }
}
