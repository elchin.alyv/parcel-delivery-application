package com.elchin.msorder.service;

import com.elchin.msorder.dto.request.AssignCourierRequestDto;
import com.elchin.msorder.dto.request.ChangeOrderDestinationRequestDto;
import com.elchin.msorder.dto.request.CreateOrderRequestDto;
import com.elchin.msorder.dto.response.OrderResponseDto;
import com.elchin.msorder.entity.Order;
import com.elchin.msorder.messaging.event.OrderStatusEvent;

import java.util.List;
import java.util.Optional;

public interface OrderService {

    OrderResponseDto getOrderById(Long id);

    List<OrderResponseDto> getAllOrders();

    List<Order> getAllOrdersByActive(boolean active);

    List<Order> getAllOrdersByCustomerUsernameAndActive(String customerUsername, boolean active);

    void createOrder(CreateOrderRequestDto requestDto);

    void changeDestinationOfOrder(ChangeOrderDestinationRequestDto requestDto);

    void cancelOrder(Long id);

    void acceptOrder(Long id);

    Optional<Order> getOrderByIdAndActive(Long id, boolean active);

    Optional<Order> getOrderByIdAndCustomerUsernameAndActive(Long id, String customerUsername, boolean active);

    Order saveOrder(Order order);

    void assignCourierToOrder(AssignCourierRequestDto requestDto);

    void changeOrderStatus(OrderStatusEvent orderStatusEvent);

}
