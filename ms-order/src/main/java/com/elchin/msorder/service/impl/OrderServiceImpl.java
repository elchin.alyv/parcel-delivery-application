package com.elchin.msorder.service.impl;

import com.elchin.msorder.dto.request.AssignCourierRequestDto;
import com.elchin.msorder.dto.request.ChangeOrderDestinationRequestDto;
import com.elchin.msorder.dto.request.CreateOrderRequestDto;
import com.elchin.msorder.dto.response.OrderResponseDto;
import com.elchin.msorder.entity.Order;
import com.elchin.msorder.enums.OrderStatus;
import com.elchin.msorder.error.exception.CommonUserException;
import com.elchin.msorder.mapper.OrderMapper;
import com.elchin.msorder.messaging.event.OrderStatusEvent;
import com.elchin.msorder.messaging.event.ParcelStatusEvent;
import com.elchin.msorder.messaging.publisher.ParcelStatusPublisher;
import com.elchin.msorder.repository.OrderRepository;
import com.elchin.msorder.security.model.AuthUser;
import com.elchin.msorder.service.EmailService;
import com.elchin.msorder.service.OrderService;
import com.elchin.msorder.util.OrderUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.elchin.msorder.enums.OrderStatus.*;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final ParcelStatusPublisher parcelStatusPublisher;
    private final EmailService emailService;
    private final AuthUser authUser;

    @Override
    public OrderResponseDto getOrderById(Long id) {
        Optional<Order> optionalOrder = authUser.getRoles().contains("ADMIN") ?
                getOrderByIdAndActive(id, true)
                :
                getOrderByIdAndCustomerUsernameAndActive(id, authUser.getUsername(), true);

        Order order = optionalOrder.orElseThrow(() -> new CommonUserException(400, "Order can not found, Order id: " + id));

        return OrderMapper.INSTANCE.toDto(order);
    }

    @Override
    public List<OrderResponseDto> getAllOrders() {
        List<Order> orders = authUser.getRoles().contains("ADMIN") ?
                getAllOrdersByActive(true)
                :
                getAllOrdersByCustomerUsernameAndActive(authUser.getUsername(), true);

        return orders.stream().map(OrderMapper.INSTANCE::toDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> getAllOrdersByActive(boolean active) {
        return orderRepository.getAllByActive(active);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> getAllOrdersByCustomerUsernameAndActive(String customerUsername, boolean active) {
        return orderRepository.getAllByCustomerUsernameAndActive(customerUsername, active);
    }

    @Override
    public void createOrder(CreateOrderRequestDto requestDto) {
        Order order = Order.builder().name(requestDto.getName())
                .description(requestDto.getDescription())
                .weight(requestDto.getWeight())
                .amount(OrderUtil.calculateOrderAmount(requestDto.getWeight()))
                .destination(requestDto.getDestination())
                .status(OrderStatus.CREATED)
                .customerUsername(authUser.getUsername())
                .active(true)
                .build();

        saveOrder(order);
    }

    @Override
    public void changeDestinationOfOrder(ChangeOrderDestinationRequestDto requestDto) {
        Long orderId = requestDto.getOrderId();
        Order order = getOrderByIdAndCustomerUsernameAndActive(orderId, authUser.getUsername(), true).
                orElseThrow(() -> new CommonUserException(400, "Order can not found, Order id: " + orderId));
        if (order.getStatus().equals(OrderStatus.CANCELED)) {
            throw new CommonUserException(400, "Order is cancelled: " + orderId);
        }
        order.setDestination(requestDto.getDestination());
        saveOrder(order);
    }

    @Override
    public void cancelOrder(Long id) {
        Optional<Order> optionalOrder = authUser.getRoles().contains("ADMIN") ?
                getOrderByIdAndActive(id, true)
                :
                getOrderByIdAndCustomerUsernameAndActive(id, authUser.getUsername(), true);

        Order order = optionalOrder.orElseThrow(() -> new CommonUserException(400, "Order can not found, Order id: " + id));

        List<OrderStatus> orderStatuses = List.of(OrderStatus.CANCELED, OrderStatus.DELIVERED);
        if (orderStatuses.contains(order.getStatus())) {
            throw new CommonUserException(400, "You can't cancel order, Order Status: " + order.getStatus());
        }

        List<OrderStatus> notifyParcel = List.of(PENDING, IN_PROGRESS);
        if (notifyParcel.contains(order.getStatus())) {
            order.setStatus(OrderStatus.CANCELED);
            publishParcelStatus(order);
        }

        order.setStatus(OrderStatus.CANCELED);
        saveOrder(order);

    }

    @Override
    public void acceptOrder(Long id) {
        Order order = getOrderByIdAndActive(id, true).
                orElseThrow(() -> new CommonUserException(400, "Order can not found, Order id: " + id));

        if (!OrderStatus.CREATED.equals(order.getStatus())) {
            throw new CommonUserException(400, "You can't accept order, Order Status: " + order.getStatus());
        }

        order.setStatus(OrderStatus.ACCEPTED);
        saveOrder(order);

        String orderName = order.getName() + " order";
        emailService.sendEmail(order.getCustomerUsername(), orderName, orderName + " is accepted");

    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Order> getOrderByIdAndActive(Long id, boolean active) {
        return orderRepository.getOrderByIdAndActive(id, active);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Order> getOrderByIdAndCustomerUsernameAndActive(Long id, String customerUsername, boolean active) {
        return orderRepository.getOrderByIdAndCustomerUsernameAndActive(id, customerUsername, true);
    }

    @Override
    public Order saveOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public void assignCourierToOrder(AssignCourierRequestDto requestDto) {
        Long orderId = requestDto.getOrderId();
        Order order = getOrderByIdAndActive(orderId, true).
                orElseThrow(() -> new CommonUserException(400, "Order can not found, Order id: " + orderId));
        if (!OrderStatus.ACCEPTED.equals(order.getStatus())) {
            throw new CommonUserException(400, "You can't assign order, Order Status: " + order.getStatus());
        }

        order.setStatus(PENDING);
        order.setCourierUsername(requestDto.getCourierUsername());
        saveOrder(order);
        publishParcelStatus(order);

        String orderName = order.getName() + " order";
        emailService.sendEmail(requestDto.getCourierUsername(), orderName, "New order assigned");

    }

    @Override
    public void changeOrderStatus(OrderStatusEvent orderStatusEvent) {
        Long orderId = orderStatusEvent.getOrderId();
        Order order = getOrderByIdAndActive(orderId, true).
                orElseThrow(() -> new CommonUserException(400, "Order can not found, Order id: " + orderId));
        if (order.getStatus().equals(PENDING) && IN_PROGRESS.equals(orderStatusEvent.getStatus())) {
            order.setStatus(IN_PROGRESS);
            saveOrder(order);
        } else if (order.getStatus().equals(IN_PROGRESS) && DELIVERED.equals(orderStatusEvent.getStatus())) {
            order.setStatus(DELIVERED);
            saveOrder(order);
        }
    }

    private void publishParcelStatus(Order order) {
        ParcelStatusEvent parcelStatusEvent = ParcelStatusEvent.builder().orderId(order.getId())
                .orderName(order.getName())
                .destination(order.getDestination())
                .amount(order.getAmount())
                .status(order.getStatus())
                .courierUsername(order.getCourierUsername())
                .customerUsername(order.getCustomerUsername())
                .build();
        parcelStatusPublisher.publishParcelStatus(parcelStatusEvent);
    }


}
