package com.elchin.msorder.controller;

import com.elchin.msorder.dto.request.AssignCourierRequestDto;
import com.elchin.msorder.dto.request.ChangeOrderDestinationRequestDto;
import com.elchin.msorder.dto.request.CreateOrderRequestDto;
import com.elchin.msorder.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
@Validated
@Api(value = "Order related services")
public class OrderController {

    private final OrderService orderService;


    @ApiOperation(value = "Service for get order detail")
    @GetMapping("/{id}")
    @PreAuthorize("@authService.checkUserAccessToResource(#jwt,'USER','ADMIN')")
    public ResponseEntity<?> getOrderDetailById(@PathVariable("id") @NotNull(message = "Id can not null") Long orderId,
                                                @RequestHeader("Authorization") @NotBlank(message = "Token expected") String jwt) {
        return ResponseEntity.ok(orderService.getOrderById(orderId));
    }

    @ApiOperation(value = "Service for get orders")
    @GetMapping
    @PreAuthorize("@authService.checkUserAccessToResource(#jwt,'USER','ADMIN')")
    public ResponseEntity<?> getOrders(@RequestHeader("Authorization") @NotBlank(message = "Token expected") String jwt) {
        return ResponseEntity.ok(orderService.getAllOrders());
    }

    @ApiOperation(value = "Service for create order")
    @PostMapping("/create")
    @PreAuthorize("@authService.checkUserAccessToResource(#jwt,'USER')")
    public ResponseEntity<?> createOrder(@RequestBody @Valid CreateOrderRequestDto requestDto,
                                         @RequestHeader("Authorization") @NotBlank(message = "Token expected") String jwt) {
        orderService.createOrder(requestDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @ApiOperation(value = "Service for change destination")
    @PutMapping("/change-destination")
    @PreAuthorize("@authService.checkUserAccessToResource(#jwt,'USER')")
    public ResponseEntity<?> changeDestination(@RequestBody @Valid ChangeOrderDestinationRequestDto requestDto,
                                               @RequestHeader("Authorization") @NotBlank(message = "Token expected") String jwt) {
        orderService.changeDestinationOfOrder(requestDto);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "Service for cancel order")
    @PutMapping("/{id}/cancel")
    @PreAuthorize("@authService.checkUserAccessToResource(#jwt,'USER','ADMIN')")
    public ResponseEntity<?> cancelOrder(@PathVariable("id") @NotNull(message = "Id can not null") Long orderId,
                                         @RequestHeader("Authorization") @NotBlank(message = "Token expected") String jwt) {
        orderService.cancelOrder(orderId);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "Service for accept order")
    @PutMapping("/{id}/accept")
    @PreAuthorize("@authService.checkUserAccessToResource(#jwt,'ADMIN')")
    public ResponseEntity<?> acceptOrder(@PathVariable("id") @NotNull(message = "Id can not null") Long orderId,
                                         @RequestHeader("Authorization") @NotBlank(message = "Token expected") String jwt) {
        orderService.acceptOrder(orderId);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "Service for assign order to courier")
    @PutMapping("/assign-courier")
    @PreAuthorize("@authService.checkUserAccessToResource(#jwt,'ADMIN')")
    public ResponseEntity<?> assignCourier(@RequestBody @Valid AssignCourierRequestDto requestDto,
                                           @RequestHeader("Authorization") @NotBlank(message = "Token expected") String jwt) {
        orderService.assignCourierToOrder(requestDto);
        return ResponseEntity.ok().build();
    }

}
