package com.elchin.msorder.dto.request;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AssignCourierRequestDto implements Serializable {

    @NotNull(message = "Order id must not be null")
    private Long orderId;
    @NotEmpty(message = "Courier username must not be empty")
    private String courierUsername;
}
