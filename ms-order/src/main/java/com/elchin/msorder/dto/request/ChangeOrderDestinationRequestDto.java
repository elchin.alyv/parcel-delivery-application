package com.elchin.msorder.dto.request;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChangeOrderDestinationRequestDto implements Serializable {

    @NotNull(message = "Order id must not be null")
    private Long orderId;
    @NotBlank(message = "Destination can not be empty")
    private String destination;

}
