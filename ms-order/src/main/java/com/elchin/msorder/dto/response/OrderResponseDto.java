package com.elchin.msorder.dto.response;

import com.elchin.msorder.enums.OrderStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderResponseDto implements Serializable {

    private Long id;
    private String name;
    private String description;
    private String destination;
    private OrderStatus status;
    private Double weight;
    private BigDecimal amount;
    private String customerUsername;
    private String courierUsername;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    private Boolean active;
}
