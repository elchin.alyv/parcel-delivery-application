package com.elchin.msorder.dto.request;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateOrderRequestDto implements Serializable {

    @NotEmpty(message = "Name must not be empty")
    private String name;
    private String description;
    @NotEmpty(message = "Destination must not be empty")
    private String destination;
    private Double weight;

}
