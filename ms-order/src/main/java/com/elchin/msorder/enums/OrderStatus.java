package com.elchin.msorder.enums;

public enum OrderStatus {
    CREATED,
    ACCEPTED,
    PENDING,
    IN_PROGRESS,
    DELIVERED,
    CANCELED;
}
