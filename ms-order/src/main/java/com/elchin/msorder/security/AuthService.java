package com.elchin.msorder.security;

import com.elchin.msorder.client.AuthFeignClient;
import com.elchin.msorder.client.dto.request.ValidateJwtRequestDto;
import com.elchin.msorder.client.dto.response.ValidateJwtResponseDto;
import com.elchin.msorder.security.model.AuthUser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class AuthService {

    private final AuthFeignClient authFeignClient;
    private final AuthUser authUser;

    public boolean checkUserAccessToResource(String jwtToken, String... role) {
        log.debug("Auth check started");
        Set<String> roles = Set.of(role);
        ValidateJwtRequestDto requestDto = ValidateJwtRequestDto.builder().jwtToken(jwtToken).roles(roles).build();
        ValidateJwtResponseDto responseDto = authFeignClient.checkAccessToResource(requestDto);
        if (Objects.nonNull(responseDto) && responseDto.isSuccess()) {
            log.debug("Success login for: {}", responseDto.getUsername());
            authUser.setUsername(responseDto.getUsername());
            authUser.setRoles(responseDto.getRoles());
            return true;
        }
        return false;
    }
}
