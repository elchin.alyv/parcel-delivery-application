package com.elchin.msorder.client.dto.request;

import lombok.*;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidateJwtRequestDto implements Serializable {

    private String jwtToken;
    private Set<String> roles;

}
