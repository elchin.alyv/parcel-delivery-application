package com.elchin.msorder.client;

import com.elchin.msorder.client.dto.request.ValidateJwtRequestDto;
import com.elchin.msorder.client.dto.response.ValidateJwtResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

@Component
@FeignClient(name = "authFeignClient", url = "${ms-auth.check-access.url}")
public interface AuthFeignClient {

    @PostMapping("/validate")
    ValidateJwtResponseDto checkAccessToResource(ValidateJwtRequestDto requestDto);
}
