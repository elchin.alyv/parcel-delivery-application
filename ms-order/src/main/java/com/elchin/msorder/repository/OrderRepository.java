package com.elchin.msorder.repository;

import com.elchin.msorder.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    Optional<Order> getOrderByIdAndActive(Long id, boolean active);

    Optional<Order> getOrderByIdAndCustomerUsernameAndActive(Long id, String username, boolean active);

    List<Order> getAllByActive(boolean active);

    List<Order> getAllByCustomerUsernameAndActive(String customerUsername, boolean active);
}
