package com.elchin.msorder.entity;

import com.elchin.msorder.enums.OrderStatus;
import lombok.*;
import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "orders")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Order extends AbstractEntity implements Serializable {

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "destination", nullable = false)
    private String destination;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private OrderStatus status;

    @Column(name = "weight")
    private Double weight;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "customer_username", nullable = false)
    private String customerUsername;

    @Column(name = "courier_username")
    private String courierUsername;

    @Column(name = "admin_username")
    private String adminUsername;

    @Column(name = "active", nullable = false)
    private Boolean active;
}
