package com.elchin.msorder.mapper;

import com.elchin.msorder.dto.response.OrderResponseDto;
import com.elchin.msorder.entity.Order;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface OrderMapper {
    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    OrderResponseDto toDto(Order order);
}
