package com.elchin.msorder.error.exception;

import lombok.Getter;

@Getter
public class CommonInternalException extends RuntimeException {

    private final Integer code;
    private final String message;

    public CommonInternalException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
