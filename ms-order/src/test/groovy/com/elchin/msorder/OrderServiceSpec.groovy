package com.elchin.msorder

import com.elchin.msorder.dto.request.AssignCourierRequestDto
import com.elchin.msorder.dto.request.ChangeOrderDestinationRequestDto
import com.elchin.msorder.dto.request.CreateOrderRequestDto
import com.elchin.msorder.entity.Order
import com.elchin.msorder.enums.OrderStatus
import com.elchin.msorder.error.exception.CommonUserException
import com.elchin.msorder.messaging.publisher.ParcelStatusPublisher
import com.elchin.msorder.repository.OrderRepository
import com.elchin.msorder.security.model.AuthUser
import com.elchin.msorder.service.EmailService
import com.elchin.msorder.service.impl.OrderServiceImpl
import spock.lang.Specification

class OrderServiceSpec extends Specification {

    private OrderRepository orderRepository = Mock();
    private ParcelStatusPublisher parcelStatusPublisher = Mock();
    private EmailService emailService = Mock();
    private AuthUser authUser = Mock();

    private OrderServiceImpl orderService;

    def setup() {
        orderService = new OrderServiceImpl(orderRepository, parcelStatusPublisher, emailService, authUser);
    }

    def "getOrderById() should return order successfully"() {
        given:
        def order = Order.builder().name("order-1").status(OrderStatus.IN_PROGRESS).build();

        when:
        def orderResponseDto = orderService.getOrderById(1)

        then:
        orderResponseDto.getName() == "order-1"
        orderResponseDto.getStatus() == OrderStatus.IN_PROGRESS
        1 * authUser.getRoles() >> Set.of("ADMIN")
        1 * orderRepository.getOrderByIdAndActive(*_) >> Optional.of(order)
    }

    def "getOrderById() should be throw exception (Order can not found)"() {
        when:
        def orderResponseDto = orderService.getOrderById(1)

        then:
        1 * authUser.getRoles() >> Set.of("ADMIN")
        1 * orderRepository.getOrderByIdAndActive(*_) >> Optional.ofNullable(null)
        def ex = thrown(CommonUserException)
        ex.getCode() == 400
        ex.getMessage() == "Order can not found, Order id: 1"
    }

    def "getAllOrders() should return all orders successfully"() {
        given:
        def order = Order.builder().name("order-1").status(OrderStatus.IN_PROGRESS).build();

        when:
        def orders = orderService.getAllOrders()

        then:
        orders.size() == 1
        orders.get(0).getName() == "order-1"
        orders.get(0).getStatus() == OrderStatus.IN_PROGRESS
        1 * authUser.getRoles() >> Set.of("ADMIN")
        1 * orderRepository.getAllByActive(*_) >> List.of(order)
    }

    def "saveOrder() should save order successfully"() {
        given:
        def order = Order.builder().name("order-1")
                .status(OrderStatus.CREATED)
                .customerUsername("elchin.alyv@gmail.com")
                .build();

        when:
        def responseOrder = orderService.saveOrder(order)

        then:
        responseOrder.getName() == "order-1"
        responseOrder.getStatus() == OrderStatus.CREATED
        responseOrder.getCustomerUsername() == "elchin.alyv@gmail.com"
        1 * orderRepository.save(*_) >> order
    }

    def "createOrder() should create order successfully"() {
        given:
        def requestDto = CreateOrderRequestDto.builder()
                .name("order-1")
                .weight(5).build();
        def order = Order.builder().name("order-1").status(OrderStatus.CREATED).build();

        when:
        orderService.createOrder(requestDto);

        then:
        1 * orderRepository.save(*_) >> order
    }

    def "changeDestinationOfOrder() should change destination successfully"() {
        given:
        def requestDto =
                ChangeOrderDestinationRequestDto.builder()
                        .orderId(1)
                        .destination("new-destination").build()
        def order = Order.builder().name("order-1").status(OrderStatus.CREATED).build()

        when:
        orderService.changeDestinationOfOrder(requestDto);

        then:
        1 * orderRepository.getOrderByIdAndCustomerUsernameAndActive(*_) >> Optional.of(order)
        1 * orderRepository.save(*_) >> order
    }

    def "assignCourierToOrder() should assign courier successfully"() {
        given:
        def requestDto =
                AssignCourierRequestDto.builder()
                        .orderId(1)
                        .courierUsername("elchin.alyv@gmail.com").build()
        def order = Order.builder().name("order-1").status(OrderStatus.ACCEPTED).build();

        when:
        orderService.assignCourierToOrder(requestDto);

        then:
        1 * orderRepository.getOrderByIdAndActive(*_) >> Optional.of(order)
        1 * orderRepository.save(*_) >> order
        1 * parcelStatusPublisher.publishParcelStatus(*_)
        1 * emailService.sendEmail(*_)
    }

    def "cancelOrder() should cancel order successfully"() {
        given:
        def order = Order.builder()
                .name("order-1")
                .status(OrderStatus.ACCEPTED).build()

        when:
        orderService.cancelOrder(1);

        then:
        1 * authUser.getRoles() >> Set.of("ADMIN")
        1 * orderRepository.getOrderByIdAndActive(*_) >> Optional.of(order)
        1 * orderRepository.save(*_) >> order
    }

    def "acceptOrder() should accept order successfully"() {
        given:
        def order = Order.builder()
                .name("order-1")
                .status(OrderStatus.CREATED).build()

        when:
        orderService.acceptOrder(1);

        then:
        1 * orderRepository.getOrderByIdAndActive(*_) >> Optional.of(order)
        1 * orderRepository.save(*_) >> order
        1 * emailService.sendEmail(*_)
    }

    def "acceptOrder() should be throw exception (Order can not found)"() {
        when:
        orderService.acceptOrder(1);

        then:
        1 * orderRepository.getOrderByIdAndActive(*_) >> Optional.ofNullable(null)
        def ex = thrown(CommonUserException)
        ex.getCode() == 400
        ex.getMessage() == "Order can not found, Order id: 1"
    }
}