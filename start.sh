cd api-gateway && ./gradlew clean build -x test && docker build -t api-gateway:1.0.0 .
cd ../ms-auth && ./gradlew clean build -x test && docker build -t ms-auth:1.0.0 .
cd ../ms-order && ./gradlew clean build -x test && docker build -t ms-order:1.0.0 .
cd ../ms-distribution && ./gradlew clean build -x test && docker build -t ms-distribution:1.0.0 .
cd ../ms-notification && ./gradlew clean build -x test && docker build -t ms-notification:1.0.0 .
cd .. && docker-compose up