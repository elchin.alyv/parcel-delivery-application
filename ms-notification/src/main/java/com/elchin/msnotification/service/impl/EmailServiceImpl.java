package com.elchin.msnotification.service.impl;

import com.elchin.msnotification.messaging.event.EmailNotificationEvent;
import com.elchin.msnotification.service.EmailService;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String from;

    @Async("fixedThreadPool")
    @Retry(name = "emailService")
    @Override
    public void sendEmail(EmailNotificationEvent email) {
        log.debug("Email started sent to: {}", email.getTo());
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(email.getTo());
        message.setText(email.getBody());
        message.setSubject(email.getSubject());
        mailSender.send(message);
    }
}
