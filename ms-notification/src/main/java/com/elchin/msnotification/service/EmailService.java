package com.elchin.msnotification.service;

import com.elchin.msnotification.messaging.event.EmailNotificationEvent;

public interface EmailService {
     void sendEmail(EmailNotificationEvent emailNotificationEvent);
}
