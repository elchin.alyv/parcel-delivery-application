package com.elchin.msnotification.messaging.subscriber;

import com.elchin.msnotification.config.RabbitMQConfig;
import com.elchin.msnotification.messaging.event.EmailNotificationEvent;
import com.elchin.msnotification.service.EmailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class NotificationSubscriber {

    private final EmailService emailService;

    @RabbitListener(queues = RabbitMQConfig.NOTIFICATION_QUEUE)
    public void consumeMessageFromQueue(EmailNotificationEvent emailNotificationEvent) {
        log.debug("Email notification event consumed, {} ", emailNotificationEvent);
//        emailService.sendEmail(emailNotificationEvent);  todo can create application email and change it in application.yml
    }
}
