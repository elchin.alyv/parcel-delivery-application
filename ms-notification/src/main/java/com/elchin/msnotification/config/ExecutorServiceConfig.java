package com.elchin.msnotification.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
@EnableAsync
public class ExecutorServiceConfig {


    @Bean("fixedThreadPool")
    public ExecutorService fixedThreadPool() {
       return Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    }
}
